import std/os
import std/json
import yaml

proc fixext(path: string): string =
  path.extractFilename().changeFileExt("json")

proc yaml2json(src, dst: string) =
  writeFile(dst, $(readFile(src).loadToJson()[0].pretty(2)))

when isMainModule:
  let cd = getCurrentDir()
  let upRegexDir = cd / "upstream" / "regexes"
  let upTestsDir = cd / "upstream" / "Tests"
  let srcDir = cd / "src" / "parser" / "regexes" / "upstream"
  let tstDir = cd / "tests" / "files"

  if not dirExists(srcDir): createDir(srcDir)
  if not dirExists(tstDir): createDir(tstDir)

  for f in walkFiles(upRegexDir / "*.yml"):
    yaml2json(f, srcDir / fixext(f))

  for f in walkFiles(upRegexDir / "**" / "*.yml"):
    let dstDir = srcDir / f.relativePath(upRegexDir).parentDir
    if not dirExists(dstDir): createDir(dstDir)
    yaml2json(f, dstDir / fixext(f))

  for f in walkFiles(upTestsDir / "fixtures" / "*.yml"):
    yaml2json(f, tstDir / fixext(f))

  for f in walkFiles(upTestsDir / "Parser" / "**" / "fixtures" / "*.yml"):
    let dstDir = tstDir / f.relativePath(upTestsDir / "Parser").parentDir.parentDir
    if not dirExists(dstDir): createDir(dstDir)
    yaml2json(f, dstDir / fixext(f))
