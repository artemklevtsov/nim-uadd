# Package

version       = "1.0.0"
author        = "Artem Klevtsov"
description   = "Nim implementation of user agent device detector"
license       = "Apache-2.0"
srcDir        = "src"
skipDirs      = @["tools", "tests"]
installExt    = @["nim"]
bin           = @["uadd"]

# Dependencies

requires "nim >= 1.6.2"
requires "cligen >= 1.5.21"
requires "yaml >= 0.16.0"
requires "jsony >= 1.1.3"
requires "regex >= 0.19.0"

task docs, "Generate documentation":
  exec "nim doc --out:public/index.html src/uadd"

task test, "Run all tests":
  exec "nim compile --run --hints:off tests/test_all"

task regex, "Make JSON from the YAML source":
  exec "nim compile --run --hints:off tools/yaml2json"

task cov, "Generate coverage report":
  exec "coco --verbose --target='tests/**/test*.nim' --cov='src' --compiler='--hints:off -d:skipTestsOutput' --report_path='public/coverage'"
