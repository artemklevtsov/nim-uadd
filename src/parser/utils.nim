import std/strutils
import std/pegs

func getMatchesSize(a: openArray[string]): int =
  result = 0
  for s in a:
    for m in s.findAll(peg "'$'{\\d+}"):
      let v = m.replace("$", "").parseInt()
      if v > result:
        result = v


template loadRegexes*(filename: string): untyped =
  when not declared RegexEntry:
    {.error: "RegexEntry type is not defined.".}
  const source {.inject.} = staticRead(filename).fromJson(seq[RegexEntry])
  var compiled {.inject, global.}: seq[Regex]
  once:
    compiled.setlen(source.len)
    for i in 0 ..< source.len:
      compiled[i] = re(source[i].regex, {reStudy})
