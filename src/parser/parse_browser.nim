import std/strutils
import std/tables
import std/re
import jsony
import utils

type
  EngineEntry = object
    default: string
    versions: Table[string, string]

  RegexEntry = object
    regex: string
    name: string
    version: string
    engine: EngineEntry

proc parseBrowser*(s: string): tuple[name: string, version: string] =
  loadRegexes("regexes/upstream/client/browsers.json")
  for i, e in source:
    var m: array[2, string]
    if s.find(compiled[i], m) != -1:
      result.name = e.name
      if e.version.len > 0:
        result.version = e.version.format(m)
      break

when isMainmodule:
  const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Amazonbot/0.1; +https://developer.amazon.com/support/amazonbot)"
  echo parseBrowser(s)
