import std/re
import jsony
import utils

type
  RegexEntry = object
    regex: string
    name: string
    category: string

proc parseBot*(s: string): tuple[name: string, category: string] =
  loadRegexes("regexes/upstream/bots.json")
  for i, e in source:
    var m: array[3, string]
    if s.find(compiled[i], m) != -1:
      result.name = e.name
      result.category = e.category
      break

when isMainmodule:
  const s = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Amazonbot/0.1; +https://developer.amazon.com/support/amazonbot)"
  assert parseBot(s) == (name: "Amazon Bot", category: "Crawler")
