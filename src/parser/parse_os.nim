import std/strutils
import std/re
import jsony
import utils

type
  VersionEntry = object
    regex: string
    version: string

  RegexEntry = object
    regex: string
    name: string
    version: string
    versions: seq[VersionEntry]

proc parseOs*(s: string): tuple[name: string, version: string] =
  loadRegexes("regexes/upstream/oss.json")
  for i, e in source:
    var m: array[3, string]
    if s.find(compiled[i], m) != -1:
      result.name = e.name.format(m)
      if e.version.len > 0:
        result.version = e.version.format(m)
      elif e.versions.len > 0:
        for v in e.versions:
          var m2: array[1, string]
          if s.find(re(v.regex), m2) != -1:
            result.version = v.version.format(m2)
            break
      break

when isMainmodule:
  const s = "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.9.0.6) Gecko/2009020414 CentOS/3.0.6-1.el5.centos Firefox/3.0.6"
  assert parseOs(s) == (name: "CentOS", version: "3.0.6")
