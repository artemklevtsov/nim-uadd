import std/re
import jsony
import utils

type
  RegexEntry = object
    regex: string
    name: string

proc parsePlatfrom*(s: string): string =
  loadRegexes("regexes/local/platform.json")
  for i, e in source:
    if s.find(compiled[i]) != -1:
      result = e.name
      break

when isMainModule:
  const s = "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.9.0.6) Gecko/2009020414 CentOS/3.0.6-1.el5.centos Firefox/3.0.6"
  assert parsePlatfrom(s) == "x64"
